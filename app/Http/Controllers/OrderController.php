<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $orders = OrderItem::all();

       return $orders->map(function ($order){
           return new OrderResource($order);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::all();
        $order = Order::create($data);

        $order->save();

        return new Response($order);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new OrderResource(OrderItem::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = OrderItem::find($id);

        if (!$order) return new Response([
            'result' => 'fail',
            'message' => 'order not found'
        ]);

        $data = Input::all();

        foreach ($data as $key => $value) {
            if ($key === 'price') {
                $order->$key = $value * 100;
            } else {
                $order->$key = $value;
            }
        }
        $order->save();
        return new Response(new OrderResource($order));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Order::destroy($id);

        return ['result' => $result ? 'success' : 'fail'];
    }

    public function deleteByBuyer($buyerId){
        $result = Order::where('buyer_id', $buyerId)->delete();
        return ['result' => $result ? 'success' : 'fail'];
    }
}
