<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
   {
       return[
           'data' => [
           'orderId' => $this->id,
           'orderDate' => $this->order->date,
           'orderSum' => $this->sum / 100,
           'orderItems' => [
               'productName' => $this->product,
               'productQty' => $this->quantity,
               'productPrice' => $this->price / 100,
               'productDiscount, %' => $this->discount,
               'productSum' => $this->price * $this->quantity / 100,
           ]],
           'buyer' => [
              'buyerFullName' => $this->order->buyer->name.' '.$this->order->buyer->surname,
             'buyerAddress' => $this->order->buyer->country.', '.$this->order->buyer->city.', '.$this->order->buyer->addressLine,
            'buyerPhone' => $this->order->buyer->phone
        ]
       ];
   }
}
