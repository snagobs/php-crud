<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $primaryKey = 'id';

    protected $fillable = ['quantity', 'price', 'order_id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
