<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'product' => $faker->text($maxNbChars = 15),
        'price' => $faker->numberBetween(100, 10000),
        'quantity' => $faker->numberBetween(1, 10),
        'discount' => $faker->numberBetween(5, 50),
        'sum' => function ($table) {
            return round($table['price'] * $table['quantity'] * (100 - $table['discount']) / 100);
        },
    ];
});
