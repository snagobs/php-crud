<?php

use Illuminate\Database\Seeder;
use App\Buyer;
use App\Order;
use App\OrderItem;
use App\Http\Controllers\OrderController;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Buyer::class, 10)->create()
            ->each(function ($buyer) {
                $buyer->orders()->saveMany(
                    factory(Order::class, 10)->create()
                        ->each(function ($order) {
                            $order->orderItems()->saveMany(
                                factory(OrderItem::class, 10)
                                    ->make(['order_id' => null]));
                        }))
                    ->make(['buyer_id' => null]);
            });;
    }
}
